<?php

namespace Cetria\Laravel\Helpers\Factory;

use Illuminate\Database\Eloquent\Relations\Relation as EloquentRelation;
use TypeError;

class Relation extends Factoryable
{
    /** @var string */
    protected $relationName;

    /**
     * @var string $relationName
     * @var int $count
     */
    public function __construct(string $relationName, int $count = 1)
    {
        parent::__construct($count);
        $this->setRelationName($relationName);
    }

    /**
     * @var string $relationName
     * @return static
     */
    public function setRelationName(string $relationName): Relation
    {
        $this->relationName = $relationName;
        return $this;
    }

    /**
     * @return string
     */
    public function getRelationName(): string
    {
        return $this->relationName;
    }

    /**
     * @param string $parentObjectClass
     * @return Model
     * @throws TypeError
     * @throws IncompatibileRelationForClassException
     */
    public function toModel(string $parentObjectClass): Model
    {
        if(!$this->isObjectModelClass($parentObjectClass)) {
            throw new TypeError();
        } elseif(!method_exists($parentObjectClass, $this->relationName)) {
            throw new IncompatibileRelationForClassException($parentObjectClass, $this->relationName);
        }
        $parentInstance = new $parentObjectClass();
        $relationMethod = $this->getRelationName();
        /** @var EloquentRelation */
        $relationObject = $parentInstance->$relationMethod();
        $newModelClass = $relationObject->getRelated();
        $modelInstance = new Model($newModelClass, $this->getCount());
        $modelInstance->addRelations($this->relations);
        return $modelInstance;
    }
}