<?php

namespace Cetria\Laravel\Helpers\Factory;

use Illuminate\Database\Eloquent\Factories\Factory;
use TypeError;
use Illuminate\Database\Eloquent\Model as ObjectModel;

class Model extends Factoryable
{
    /** @var string */
    protected $modelClass;

    /**
     * @param ObjectModel|string $objectOrClass
     * @param int $count 
     * @throws TypeError
     */
    public function __construct($objectOrClass, int $count = 1)
    {
        parent::__construct($count);
        $this->setObjectOrClass($objectOrClass);
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->modelClass;
    }

    /**
     * @param ObjectModel|string $objectOrClass
     * @return static
     * @throws TypeError
     */
    public function setObjectOrClass($objectOrClass): Model
    {
        if($this->isObjectModelInstance($objectOrClass)) {
            $this->modelClass = \get_class($objectOrClass);
        } elseif ($this->isObjectModelClass($objectOrClass)) {
            $this->modelClass = $objectOrClass;
        } else {
            throw new TypeError();
        }
        return $this;
    }

    private function isObjectModelInstance($objectOrClass): bool
    {
        return $objectOrClass instanceof ObjectModel;
    }

    public function toFactory(): Factory
    {
        $class = $this->modelClass;
        $factory = $class::factory()
            ->count($this->count);
        foreach($this->getRelations() as $relationObject) {
            $relationModel = $relationObject->toModel($class);
            $factory = $factory->has(
                $relationModel->toFactory(),
                $relationObject->getRelationName()
            );
        }
        return $factory;
    }
}