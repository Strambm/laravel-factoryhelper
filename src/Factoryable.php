<?php

namespace Cetria\Laravel\Helpers\Factory;

use Illuminate\Database\Eloquent\Model as ObjectModel;
use TypeError;

abstract class Factoryable
{
    /** @var int */
    protected $count;
    /** @var Relation[] */
    protected $relations;

    /**
     * @param int $count
     */
    public function __construct(int $count = 1)
    {
        $this->cleanRelations();
        $this->setCount($count);
    }

    public function __toString(): string
    {
        return json_encode($this->toArray());
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param int $count more than 0
     * @return static
     */
    public function setCount(int $count): Factoryable
    {
        $minValue = 1;
        if($count > $minValue) {
            $this->count = $count;
        } elseif($this->count < $minValue) {
            $this->count = 1;
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param Relation $relation
     * @return static
     */
    public function addRelation(Relation $relation): Factoryable
    {
        $this->relations[] = $relation;
        return $this;
    }

    /**
     * @param Relation[] $relations
     * @return static
     * @throws TypeError
     */
    public function addRelations(array $relations): Factoryable
    {
        foreach($relations as $relation) {
            $this->addRelation($relation);
        }
        return $this;
    }

    /**
     * @return static
     */
    public function cleanRelations(): Factoryable
    {
        $this->relations = [];
        return $this;
    }

    /**
     * @return Relation[]
     */
    public function getRelations(): array
    {
        return $this->relations;
    }

    /**
     * @param mixed $var
     * @return bool
     */
    protected function isObjectModelClass($var): bool
    {
        return \is_string($var)
                && \is_subclass_of($var, ObjectModel::class);
    }
}