<?php

namespace Cetria\Laravel\Helpers\Factory;

use Exception;

class IncompatibileRelationForClassException extends Exception
{
    public function __construct(string $class, string $relation, int $code = 0, $previous = null)
    {
        parent::__construct($this->msg($class, $relation), $code, $previous);
    }

    private function msg(string $class, string $relation): string
    {
        return 'Incompatible relation: \'' . $relation . '\' for class: \'' . $class . '\'; Method doesnt exist!';
    }
}