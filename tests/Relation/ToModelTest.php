<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Relation;

use TypeError;
use function count;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Factory\Model;
use Cetria\Laravel\Helpers\Test\Dummy\User;
use Cetria\Laravel\Helpers\Factory\Relation;
use Cetria\Laravel\Helpers\Test\Dummy\Order;
use Cetria\Laravel\Helpers\Test\Dummy\Basket;
use Cetria\Laravel\Helpers\Factory\IncompatibileRelationForClassException;

class ToModelTest extends TestCase
{
    public function testMethod__withRelation(): void
    {
        $basketCount = 10;
        $relation = (new Relation('basketItems', $basketCount))->addRelation(new Relation('product', 1));
        $model = $relation->toModel(Order::class);
        $this->assertInstanceOf(Model::class, $model);
        $returnedModelClass = Reflection::getHiddenProperty($model, 'modelClass');
        $returnedRelations = Reflection::getHiddenProperty($model, 'relations');
        $returnedCount = Reflection::getHiddenProperty($model, 'count');
        $this->assertEquals(Basket::class, $returnedModelClass);
        $this->assertEquals(1, count($returnedRelations));
        $this->assertEquals($basketCount, $returnedCount);
    }

    public function testMethod__incompatibileClass(): void
    {
        $relation = (new Relation('basketItems'))->addRelation(new Relation('product'));
        $this->expectException(IncompatibileRelationForClassException::class);
        $relation->toModel(User::class);
    }

    public function testMethod__nonEloquentModelClass(): void
    {
        $relation = (new Relation('basketItems'))->addRelation(new Relation('product'));
        $this->expectException(TypeError::class);
        $relation->toModel(self::class);
    }
}