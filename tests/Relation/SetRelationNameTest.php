<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Relation;

use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Factory\Relation;

class SetRelationNameTest extends TestCase
{
    public function testMethod(): void
    {
        $relationName = 'relationName';
        $instance = new Relation('tmp');
        $instance->setRelationName($relationName);
        $storedRelation = Reflection::getHiddenProperty($instance, 'relationName');
        $this->assertEquals($relationName, $storedRelation);
    }
}