<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Relation;

use function rand;
use function count;
use function is_array;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Factory\Relation;

class ConstructTest extends TestCase
{
    public function testMethod__defaultParams(): void
    {
        $relationName = 'test';
        $instance = new Relation($relationName);
        $this->assertMethod($instance, $relationName, 1);
    }

    public function testMethod__setCount(): void
    {
        $value = rand(2, 50);
        $relationName = 'test';
        $instance = new Relation($relationName, $value);
        $this->assertMethod($instance, $relationName, $value);
    }

    public function testMethod__setCountLowerThan1(): void
    {
        $value = rand(-50, 0);
        $relationName = 'test';
        $instance = new Relation($relationName, $value);
        $this->assertMethod($instance, $relationName, 1);
    }

    private function assertMethod(Relation $relation, string $relationName, int $count): void
    {
        $storedCount = Reflection::getHiddenProperty($relation, 'count');
        $storedRelations = Reflection::getHiddenProperty($relation, 'relations');
        $storedRelation = Reflection::getHiddenProperty($relation, 'relationName');
        $this->assertEquals($count, $storedCount);
        $this->assertTrue(is_array($storedRelations));
        $this->assertEquals(0, count($storedRelations));
        $this->assertEquals($relationName, $storedRelation);
    }
}