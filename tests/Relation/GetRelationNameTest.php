<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Relation;

use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Factory\Relation;
use Cetria\Laravel\Helpers\Test\Dummy\Product;

class GetRelationNameTest extends TestCase
{
    public function testMethod(): void
    {
        $relation = new Relation(Product::class);
        $expectedRelationName = 'DummyMethod';
        Reflection::setHiddenProperty($relation, 'relationName', $expectedRelationName);
        $returnedRelation = $relation->getRelationName();
        $this->assertEquals($expectedRelationName, $returnedRelation);
    }
}