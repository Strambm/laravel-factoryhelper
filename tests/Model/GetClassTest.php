<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Model;

use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Factory\Model;
use Cetria\Laravel\Helpers\Test\Dummy\Product;

class GetClassTest extends TestCase
{
    public function testMethod__withEloquentModelInstance(): void
    {
        $model = new Model(Product::class);
        $expectedClass = 'DummyClass';
        Reflection::setHiddenProperty($model, 'modelClass', $expectedClass);
        $returnedClass = $model->getClass();
        $this->assertEquals($expectedClass, $returnedClass);
    }
}