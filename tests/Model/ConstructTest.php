<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Model;

use TypeError;
use function rand;
use function count;
use function is_array;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Factory\Model;
use Illuminate\Database\Eloquent\Model as EloquentModel;

class ConstructTest extends TestCase
{
    public function testMethod__defaultParams(): void
    {
        $eloquentModelClass = get_class($this->getInstanceOfEloquentModel());
        $instance = new Model($eloquentModelClass);
        $this->assertMethod($instance, $eloquentModelClass, 1);
    }

    public function testMethod__defaultParams__badModelClass(): void
    {
        $eloquentModelClass = get_class(new class(){});
        $this->expectException(TypeError::class);
        new Model($eloquentModelClass);
    }

    public function testMethod__setCount(): void
    {
        $value = rand(2, 50);
        $eloquentModelClass = get_class($this->getInstanceOfEloquentModel());
        $instance = new Model($eloquentModelClass, $value);
        $this->assertMethod($instance, $eloquentModelClass, $value);
    }

    public function testMethod__setCountLowerThan1(): void
    {
        $value = rand(-50, 0);
        $eloquentModelClass = get_class($this->getInstanceOfEloquentModel());
        $instance = new Model($eloquentModelClass, $value);
        $this->assertMethod($instance, $eloquentModelClass, 1);
    }

    private function getInstanceOfEloquentModel(): EloquentModel
    {
        return new class extends EloquentModel {

        };
    }

    private function assertMethod(Model $model, string $expectedClass, int $count): void
    {
        $storedCount = Reflection::getHiddenProperty($model, 'count');
        $storedRelations = Reflection::getHiddenProperty($model, 'relations');
        $storedModel = Reflection::getHiddenProperty($model, 'modelClass');
        $this->assertEquals($count, $storedCount);
        $this->assertTrue(is_array($storedRelations));
        $this->assertEquals(0, count($storedRelations));
        $this->assertEquals($expectedClass, $storedModel);
    }
}