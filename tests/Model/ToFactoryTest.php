<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Model;

use function rand;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Factory\Model;
use Cetria\Laravel\Helpers\Factory\Relation;
use Cetria\Laravel\Helpers\Test\Dummy\Order;
use Cetria\Laravel\Helpers\Test\Dummy\Basket;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;
use Illuminate\Database\Eloquent\Factories\Factory;

class ToFactoryTest extends TestCase
{
    public function testMethod(): void
    {
        DummyHelper::init();
        $amount = rand(2, 20);
        $class = Product::class;
        $instance = new Model($class, $amount);
        $factory = $instance->toFactory();
        $this->assertFactory($factory, $class, $amount);
    }

    public function testMethod__withRelation(): void
    {
        DummyHelper::init();
        $basketCount = 10;
        $instance = new Model(Order::class);
        $instance->addRelation((new Relation('basketItems', $basketCount))->addRelation(new Relation('product', 1)));
        $factory = $instance->toFactory();
        //first dimension
        $this->assertFactory($factory, Order::class, 1);
        $relations = Reflection::getHiddenProperty($factory, 'has');
        $this->assertEquals(1, $relations->count());
        //seccond dimension
        $basketFactory = Reflection::getHiddenProperty($relations->first(), 'factory');
        $this->assertFactory($basketFactory, Basket::class, $basketCount);
        $relations = Reflection::getHiddenProperty($basketFactory, 'has');
        $this->assertEquals(1, $relations->count());
        //third dimension
        $productFactory = Reflection::getHiddenProperty($relations->first(), 'factory');
        $this->assertFactory($productFactory, Product::class, 1);
        $relations = Reflection::getHiddenProperty($factory, 'has');
        $this->assertEquals(1, $relations->count());
    }

    private function assertFactory(Factory $factory, string $expectedModelClass, int $expectedCount): void
    {
        $count = Reflection::getHiddenProperty($factory, 'count');
        $this->assertEquals($expectedCount, $count);
        $modelClass = Reflection::getHiddenProperty($factory, 'model');
        $this->assertEquals($expectedModelClass, $modelClass);
    }
}