<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Model;

use TypeError;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Factory\Model;
use Illuminate\Database\Eloquent\Model as EloquentModel;

class SetObjectOrClassTest extends TestCase
{
    public function testMethod__withEloquentModelInstance(): void
    {
        $eloquentModelDefault = $this->getInstanceOfEloquentModel();
        $instance = new Model($eloquentModelDefault);
        $eloquentModel = $this->getInstanceOfEloquentModel();
        $instance->setObjectOrClass($eloquentModel);
        $storedRelations = Reflection::getHiddenProperty($instance, 'modelClass');
        $this->assertEquals(get_class($eloquentModel), $storedRelations);
    }

    public function testMethod__withEloquentModelClass(): void
    {
        $eloquentModelDefault = $this->getInstanceOfEloquentModel();
        $instance = new Model($eloquentModelDefault);
        $eloquentModel = $this->getInstanceOfEloquentModel();
        $instance->setObjectOrClass(get_class($eloquentModel));
        $storedRelations = Reflection::getHiddenProperty($instance, 'modelClass');
        $this->assertEquals(get_class($eloquentModel), $storedRelations);
    }

    public function testMethod__withNonEloquentModelInstance(): void
    {
        $eloquentModelDefault = $this->getInstanceOfEloquentModel();
        $instance = new Model($eloquentModelDefault);
        $eloquentModel = $this->getInstanceOfModel();
        $this->expectException(TypeError::class);
        $instance->setObjectOrClass($eloquentModel);
    }

    private function getInstanceOfEloquentModel(): EloquentModel
    {
        return new class extends EloquentModel {

        };
    }

    private function getInstanceOfModel(): object
    {
        return new class {};
    }
}