<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Factoryable;

use stdClass;
use TypeError;
use function count;
use function is_array;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Factory\Relation;
use Cetria\Laravel\Helpers\Factory\Factoryable;

class AddRelationsTest extends TestCase
{
    public function testMethod(): void
    {
        $relations = [
            new Relation('fakeRel'),
            new Relation('fakeRe2'),
            new Relation('fakeRel')
        ];
        $stub = $this->getMockForAbstractClass(Factoryable::class);
        $stub->addRelations($relations);
        $storedRelations = Reflection::getHiddenProperty($stub, 'relations');
        $this->assertTrue(is_array($relations));
        $this->assertEquals(count($relations), count($storedRelations));
        foreach($relations as $key => $relation) {
            $this->assertEquals($relation, $storedRelations[$key]);
        }
    }

    public function testMethod__throwTypeException(): void
    {
        $relations = [
            new stdClass,
        ];
        $stub = $this->getMockForAbstractClass(Factoryable::class);
        $this->expectException(TypeError::class);
        $stub->addRelations($relations);
    }
}