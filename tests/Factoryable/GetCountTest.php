<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Factoryable;

use function rand;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Factory\Factoryable;

class GetCountTest extends TestCase
{
    public function testMethod(): void
    {
        $stub = $this->getMockForAbstractClass(Factoryable::class);
        $count = rand(2, 50);
        Reflection::setHiddenProperty($stub, 'count', $count);
        $returned = $stub->getCount();
        $this->assertEquals($count, $returned);
    }
}