<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Factoryable;

use function rand;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Factory\Factoryable;

class SetCountTest extends TestCase
{
    public function testMethod(): void
    {
        $stub = $this->getMockForAbstractClass(Factoryable::class);
        $count = rand(2, 50);
        $stub->setCount($count);
        $stored = Reflection::getHiddenProperty($stub, 'count');
        $this->assertEquals($count, $stored);
    }

    public function testMethod__lowerThan1(): void
    {
        $stub = $this->getMockForAbstractClass(Factoryable::class);
        $count = rand(-50, 0);
        $stub->setCount($count);
        $stored = Reflection::getHiddenProperty($stub, 'count');
        $this->assertEquals(1, $stored);
    }
}