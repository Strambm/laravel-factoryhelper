<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Factoryable;

use function count;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Helpers\Factory\Factoryable;
use Cetria\Laravel\Helpers\Factory\Relation;

class GetRelationsTest extends TestCase
{
    public function testMethod(): void
    {
        $relations = [
            new Relation('fakeRel'),
            new Relation('fakeRe2'),
            new Relation('fakeRel')
        ];
        $stub = $this->getMockForAbstractClass(Factoryable::class);
        Reflection::setHiddenProperty($stub, 'relations', $relations);
        $storedRelations = $stub->getRelations();
        $this->assertEquals(count($relations), count($storedRelations));
        foreach($relations as $key => $relation) {
            $this->assertEquals($relation, $storedRelations[$key]);
        }
    }
}