<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Factoryable;

use function rand;
use function count;
use function is_array;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Factory\Factoryable;

class ConstructTest extends TestCase
{
    public function testMethod__defaultParams(): void
    {
        $stub = $this->getMockForAbstractClass(Factoryable::class);
        $storedCount = Reflection::getHiddenProperty($stub, 'count');
        $storedRelations = Reflection::getHiddenProperty($stub, 'relations');
        $this->assertEquals(1, $storedCount);
        $this->assertTrue(is_array($storedRelations));
        $this->assertEquals(0, count($storedRelations));
    }

    public function testMethod__setCount(): void
    {
        $value = rand(2, 50);
        $stub = $this->getMockForAbstractClass(Factoryable::class, [$value]);
        $stored = Reflection::getHiddenProperty($stub, 'count');
        $this->assertEquals($value, $stored);
    }

    public function testMethod__setCountLowerThan1(): void
    {
        $value = rand(-50, 0);
        $stub = $this->getMockForAbstractClass(Factoryable::class, [$value]);
        $stored = Reflection::getHiddenProperty($stub, 'count');
        $this->assertEquals(1, $stored);
    }
}