<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Factoryable;

use function count;
use function is_array;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Helpers\Factory\Factoryable;
use Cetria\Laravel\Helpers\Factory\Relation;

class CleanRelationsTest extends TestCase
{
    public function testMethod(): void
    {
        $relations = [
            new Relation('fakeRel'),
            new Relation('fakeRe2'),
            new Relation('fakeRel')
        ];
        $stub = $this->getMockForAbstractClass(Factoryable::class);
        Reflection::setHiddenProperty($stub, 'relations', $relations);
        $stub->cleanRelations();
        $storedRelations = Reflection::getHiddenProperty($stub, 'relations');
        $this->assertTrue(is_array($relations));
        $this->assertEquals(0, count($storedRelations));
    }
}