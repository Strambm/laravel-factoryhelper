<?php

namespace Cetria\Laravel\Helpers\Factory\Tests\Factoryable;

use function current;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Factory\Relation;
use Cetria\Laravel\Helpers\Factory\Factoryable;

class AddRelationTest extends TestCase
{
    public function testMethod(): void
    {
        $relation = new Relation('fakeRel');
        $stub = $this->getMockForAbstractClass(Factoryable::class);
        $stub->addRelation($relation);
        $relations = Reflection::getHiddenProperty($stub, 'relations');
        $this->assertTrue(is_array($relations));
        $this->assertEquals(1, count($relations));
        $this->assertEquals($relation, current($relations));
    }
}